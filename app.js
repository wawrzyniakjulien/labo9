const app = require('express')();
const express = require('express');
const http = require('http').Server(app);
const io = require('socket.io')(http);
const uniqid = require('uniqid');

app.use(express.static('views'));

const users = {};

io.on('connection', function(socket){
    console.log('New user connected');
    socket.on('user', function (data) {
        if (data.name !== undefined) {
            const token = uniqid();
            users[token] = data.name;
            socket.emit('connected', {
                token
            });
            socket.emit('message', {
                from: 'INFOS',
                message: `Welcome ${data.name}!`
            })
        }
    });
    socket.on('message', (data) => {
        if (users[data.token] !== undefined) {
            io.sockets.emit('message', {
                from: users[data.token],
                message: data.message
            });
        }
    });
});

http.listen(3000, function(){
    console.log('listening on localhost:3000');
});